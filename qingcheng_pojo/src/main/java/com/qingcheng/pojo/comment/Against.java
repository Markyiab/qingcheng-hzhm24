package com.qingcheng.pojo.comment;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * against实体类
 * @author Administrator
 *
 */
@Table(name="tb_against")
public class Against implements Serializable{

	@Id
	private String id;//投诉编号

	private String username;//发起用户

	private String orderitemId;//被投诉评论id

	private String reason;//理由

	private java.util.Date createTime;//生成时间

	private java.util.Date auditTime;//审核时间

	private String status;//审核状态0未审核,1审核隐藏,2驳回

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getOrderitemId() {
		return orderitemId;
	}
	public void setOrderitemId(String orderitemId) {
		this.orderitemId = orderitemId;
	}

	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	public java.util.Date getAuditTime() {
		return auditTime;
	}
	public void setAuditTime(java.util.Date auditTime) {
		this.auditTime = auditTime;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}


	
}

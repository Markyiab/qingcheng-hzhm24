package com.qingcheng.pojo.comment;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * commentSon实体类
 * @author Administrator
 *
 */
@Table(name="tb_comment_son")
public class CommentSon implements Serializable{

	@Id
	private String id;//子评论id


	

	private String username;//用户名

	private String usernameReply;//回复对象

	private java.util.Date createTime;//评论时间

	private String parentId;//父评论id

	private String commentInfo;//评论详情

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsernameReply() {
		return usernameReply;
	}
	public void setUsernameReply(String usernameReply) {
		this.usernameReply = usernameReply;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getCommentInfo() {
		return commentInfo;
	}
	public void setCommentInfo(String commentInfo) {
		this.commentInfo = commentInfo;
	}


	
}

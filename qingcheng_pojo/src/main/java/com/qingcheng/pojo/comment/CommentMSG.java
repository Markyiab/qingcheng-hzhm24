package com.qingcheng.pojo.comment;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

public class CommentMSG implements Serializable{
    /**
     * comment包装实体类
     * @author Administrator
     *
     */
    private Comment comment;
    private boolean ifP=false;//是否点赞

    public CommentMSG(Comment comment) {
        this.comment = comment;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public boolean isIfP() {
        return ifP;
    }

    public void setIfP(boolean ifP) {
        this.ifP = ifP;
    }
}

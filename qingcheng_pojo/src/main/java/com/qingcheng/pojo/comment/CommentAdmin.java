package com.qingcheng.pojo.comment;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * commentAdmin实体类
 * @author Administrator
 *
 */
@Table(name="tb_comment_admin")
public class CommentAdmin implements Serializable{

	@Id
	private String id;//主键id


	

	private String parentId;//父评论id

	private String content;//回复内容

	private String adminId;//客服id

	private java.util.Date createTime;//评论创建时间

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public String getAdminId() {
		return adminId;
	}
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}


	
}

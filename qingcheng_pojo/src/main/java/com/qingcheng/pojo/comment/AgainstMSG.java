package com.qingcheng.pojo.comment;

import java.io.Serializable;
import java.util.Date;

/**
 * against包装实体类
 * AgainstMSG
 */
public class AgainstMSG implements Serializable {
    private String againstid;
    private String username;
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAgainstid() {
        return againstid;
    }

    public void setAgainstid(String againstid) {
        this.againstid = againstid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

package com.qingcheng.pojo.comment;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * comment实体类
 * @author Administrator
 *
 */
@Table(name="tb_comment")
public class Comment implements Serializable{

	@Id
	private String id;//评论id（等同订单详情Id）

	private String spuId;//spu的id

	private String skuId;//sku的id

	private String spec;//商品规格信息

	private String isHasimg;//是否有图:0无,1有

	private Integer userEvaluation;//数字1-5星

	private String evaluationRating;//评论等级:0差评1中评2好评

	private java.util.Date createTime;//评论创建时间

	private Integer praiseNum;//点赞数

	private String isShow;//评论是否隐藏:0隐藏,1显示

	private String isSuperadditionid;//追评状态:0无,1有追评,2无官方回复3有官方回复

	private String followComment;//将追评设置为JSON

	private String username;//用户名

	private String images;//图片地址

	private String commentContent;//文字评论

	private Integer replyNum;//回复数量

	private String isDelete;//逻辑删除:0未删除,1已删除

	private String isHide;//是否匿名:0不匿名1匿名


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getSpuId() {
		return spuId;
	}
	public void setSpuId(String spuId) {
		this.spuId = spuId;
	}

	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public String getSpec() {
		return spec;
	}
	public void setSpec(String spec) {
		this.spec = spec;
	}

	public String getIsHasimg() {
		return isHasimg;
	}
	public void setIsHasimg(String isHasimg) {
		this.isHasimg = isHasimg;
	}

	public Integer getUserEvaluation() {
		return userEvaluation;
	}
	public void setUserEvaluation(Integer userEvaluation) {
		this.userEvaluation = userEvaluation;
	}

	public String getEvaluationRating() {
		return evaluationRating;
	}
	public void setEvaluationRating(String evaluationRating) {
		this.evaluationRating = evaluationRating;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	public Integer getPraiseNum() {
		return praiseNum;
	}
	public void setPraiseNum(Integer praiseNum) {
		this.praiseNum = praiseNum;
	}

	public String getIsShow() {
		return isShow;
	}
	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

	public String getIsSuperadditionid() {
		return isSuperadditionid;
	}
	public void setIsSuperadditionid(String isSuperadditionid) {
		this.isSuperadditionid = isSuperadditionid;
	}

	public String getFollowComment() {
		return followComment;
	}
	public void setFollowComment(String followComment) {
		this.followComment = followComment;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getImages() {
		return images;
	}
	public void setImages(String images) {
		this.images = images;
	}

	public String getCommentContent() {
		return commentContent;
	}
	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}

	public Integer getReplyNum() {
		return replyNum;
	}
	public void setReplyNum(Integer replyNum) {
		this.replyNum = replyNum;
	}

	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public String getIsHide() {
		return isHide;
	}
	public void setIsHide(String isHide) {
		this.isHide = isHide;
	}


	@Override
	public String toString() {
		return "Comment{" +
				"id='" + id + '\'' +
				", spuId='" + spuId + '\'' +
				", skuId='" + skuId + '\'' +
				", spec='" + spec + '\'' +
				", isHasimg='" + isHasimg + '\'' +
				", userEvaluation=" + userEvaluation +
				", evaluationRating='" + evaluationRating + '\'' +
				", createTime=" + createTime +
				", praiseNum=" + praiseNum +
				", isShow='" + isShow + '\'' +
				", isSuperadditionid='" + isSuperadditionid + '\'' +
				", followComment='" + followComment + '\'' +
				", username='" + username + '\'' +
				", images='" + images + '\'' +
				", commentContent='" + commentContent + '\'' +
				", replyNum=" + replyNum +
				", isDelete='" + isDelete + '\'' +
				", isHide='" + isHide + '\'' +
				'}';
	}
}

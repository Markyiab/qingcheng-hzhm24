package com.qingcheng.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.entity.Result;
import com.qingcheng.pojo.comment.Against;
import com.qingcheng.service.comment.AgainstService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/against")
public class AgainstController {
    @Reference
    private AgainstService AgainstService;
    /**
     * ZXC;接口POST:/against/add.do
     * 举报评论
     * @param against 举报实体类
     */
    @PostMapping("/add")
    public Result add(@RequestBody Against against){
        against.setUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        AgainstService.add(against);
        return new Result();
    }
}

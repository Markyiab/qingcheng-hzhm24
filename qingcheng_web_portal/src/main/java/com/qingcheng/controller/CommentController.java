package com.qingcheng.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.entity.Result;
import com.qingcheng.pojo.comment.Comment;
import com.qingcheng.service.comment.CommentService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * @author: create by Yao ZhongZhe
 * @date:2019/7/29
 * @version: v1.0
 */
@RestController
@RequestMapping("/comment")
public class CommentController {

    @Reference
    private CommentService commentService;

    @PostMapping("/add")
    public Result add(@RequestBody Comment comment){
        System.out.println(comment);
        String username= SecurityContextHolder.getContext().getAuthentication().getName();
        comment.setUsername(username);
        commentService.add(comment);
        return new Result();
    }

    @PostMapping("/again")
    public Result again(@RequestBody Comment comment){
        String username= SecurityContextHolder.getContext().getAuthentication().getName();
        comment.setUsername(username);
        commentService.update(comment);
        return new Result();
    }
    /**
     * ZXC;接口GET：/comment/praise.do
     *用户点赞/取消点赞
     *@param:id(点赞评论id)
     */
    @GetMapping("/praise")
    public Result praise(String id){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        commentService.praise(username,id);
        return new Result();
    }



}

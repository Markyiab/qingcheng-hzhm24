package com.qingcheng.service.comment;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.comment.CommentAdmin;

import java.util.*;

/**
 * commentAdmin业务逻辑层
 */
public interface CommentAdminService {


    public List<CommentAdmin> findAll();


    public PageResult<CommentAdmin> findPage(int page, int size);


    public List<CommentAdmin> findList(Map<String, Object> searchMap);


    public PageResult<CommentAdmin> findPage(Map<String, Object> searchMap, int page, int size);


    public CommentAdmin findById(String id);

    public void add(CommentAdmin commentAdmin);


    public void update(CommentAdmin commentAdmin);


    public void delete(String id);

    void add(String username,String id, String content);
}

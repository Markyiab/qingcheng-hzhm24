package com.qingcheng.service.comment;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.comment.Against;

import java.util.*;

/**
 * against业务逻辑层
 */
public interface AgainstService {


    public List<Against> findAll();


    public PageResult<Against> findPage(int page, int size);


    public List<Against> findList(Map<String, Object> searchMap);


    public PageResult<Against> findPage(Map<String, Object> searchMap, int page, int size);


    public Against findById(String id);

    public void add(Against against);


    public void update(Against against);


    public void delete(String id);

    void update(String id,String orderitemId, String status);
}

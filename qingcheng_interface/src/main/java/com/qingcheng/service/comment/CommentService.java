package com.qingcheng.service.comment;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.comment.Comment;

import java.util.*;

/**
 * comment业务逻辑层
 */
public interface CommentService {

    public void praise (String username,String id);


    public List<Comment> findAll();


    public PageResult<Comment> findPage(int page, int size);


    public List<Comment> findList(Map<String, Object> searchMap);


    public PageResult<Comment> findPage(Map<String, Object> searchMap, int page, int size);


    public Comment findById(String id);

    public void add(Comment comment);


    public void update(Comment comment);


    public void delete(String id);

    void addagain(String id, String followComment);

//    Comment findStatus(String id);
}

package com.qingcheng.service.comment;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.comment.CommentSon;

import java.util.*;

/**
 * commentSon业务逻辑层
 */
public interface CommentSonService {


    public List<CommentSon> findAll();


    public PageResult<CommentSon> findPage(int page, int size);


    public List<CommentSon> findList(Map<String, Object> searchMap);


    public PageResult<CommentSon> findPage(Map<String, Object> searchMap, int page, int size);


    public CommentSon findById(String id);

    public void add(CommentSon commentSon);


    public void update(CommentSon commentSon);


    public void delete(String id);

}

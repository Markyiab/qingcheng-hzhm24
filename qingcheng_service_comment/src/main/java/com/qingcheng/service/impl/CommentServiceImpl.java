package com.qingcheng.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qingcheng.dao.CommentMapper;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.comment.Comment;
import com.qingcheng.pojo.comment.CommentMSG;
import com.qingcheng.pojo.order.Order;
import com.qingcheng.pojo.order.OrderItem;
import com.qingcheng.service.comment.CommentService;
import com.qingcheng.service.order.OrderItemService;
import com.qingcheng.service.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Reference
    private OrderService orderService;

    @Reference
    private OrderItemService orderItemService;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * ZXC用户点赞
     * @param username
     * @param id
     */
    @Override
    public void praise(String username, String id) {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder append = stringBuilder.append(id).append(username);
        String s = append.toString();
        if (redisTemplate.boundHashOps("P_U").hasKey(s)){
            System.out.println(username+"取消点赞");
            redisTemplate.boundHashOps("P_U").delete(s);
            Comment comment = new Comment();
            comment.setId(id);
            comment = commentMapper.selectByPrimaryKey(comment);
            Integer praiseNum = comment.getPraiseNum()-1;
            praiseNum=praiseNum<0?0:praiseNum;
            comment.setPraiseNum(praiseNum);
            commentMapper.updateByPrimaryKey(comment);
        }else {
            System.out.println(username+"点赞");
            redisTemplate.boundHashOps("P_U").put(s,new Date());
            Comment comment = new Comment();
            comment.setId(id);
            comment = commentMapper.selectByPrimaryKey(comment);
            Integer praiseNum = comment.getPraiseNum();
            comment.setPraiseNum(praiseNum+1);
            commentMapper.updateByPrimaryKey(comment);
        }
    }
    /**
     * ZXC判断用户点赞
     * @param comment 未判断过的
     * @param username 用户名
     * @return commentMSG 判断过的
     */
    private CommentMSG ifPraise(Comment comment,String username){
        String id = comment.getId();
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder append = stringBuilder.append(id).append(username);
        String s = append.toString();
        CommentMSG commentMSG = new CommentMSG(comment);
        if (redisTemplate.boundHashOps("P_U").hasKey(s)){
            commentMSG.setIfP(true);
        }
        return commentMSG;
    }

    /**
     * 返回全部记录
     * @return
     */
    @Override
    public List<Comment> findAll() {
        return commentMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    @Override
    public PageResult<Comment> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Comment> comments = (Page<Comment>) commentMapper.selectAll();
        return new PageResult<Comment>(comments.getTotal(),comments.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    @Override
    public List<Comment> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return commentMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageResult<Comment> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Comment> comments = (Page<Comment>) commentMapper.selectByExample(example);
        return new PageResult<Comment>(comments.getTotal(),comments.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    @Override
    public Comment findById(String id) {
        return commentMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param comment
     */
    @Override
    public void add(Comment comment) {
        System.out.println("----------------------");
        System.out.println(comment);
        comment.setCreateTime(new Date());
        commentMapper.insert(comment);


        OrderItem orderItem = orderItemService.findById(comment.getId());
        System.out.println("----------------------");
        Order order = new Order();
        order.setId(orderItem.getOrderId());
        order.setBuyerRate("1");
        orderService.update(order);
    }

    /**
     * 修改
     * @param comment
     */
    @Override
    public void update(Comment comment) {
        commentMapper.updateByPrimaryKeySelective(comment);
    }

    /**
     *  删除
     * @param id
     */
    @Override
    public void delete(String id) {
        commentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void addagain(String id, String followComment) {

    }


    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Comment.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 评论id（等同订单详情Id）
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // spu的id
            if(searchMap.get("spuId")!=null && !"".equals(searchMap.get("spuId"))){
                criteria.andLike("spuId","%"+searchMap.get("spuId")+"%");
            }
            // sku的id
            if(searchMap.get("skuId")!=null && !"".equals(searchMap.get("skuId"))){
                criteria.andLike("skuId","%"+searchMap.get("skuId")+"%");
            }
            // 是否有图:0无,1有
            if(searchMap.get("isHasimg")!=null && !"".equals(searchMap.get("isHasimg"))){
                criteria.andLike("isHasimg","%"+searchMap.get("isHasimg")+"%");
            }
            // 评论等级:0差评1中评2好评
            if(searchMap.get("evaluationRating")!=null && !"".equals(searchMap.get("evaluationRating"))){
                criteria.andLike("evaluationRating","%"+searchMap.get("evaluationRating")+"%");
            }
            // 评论是否隐藏:0隐藏,1显示
            if(searchMap.get("isShow")!=null && !"".equals(searchMap.get("isShow"))){
                criteria.andLike("isShow","%"+searchMap.get("isShow")+"%");
            }
            // 追评状态:0无,1有追评,2无官方回复3有官方回复
            if(searchMap.get("isSuperadditionid")!=null && !"".equals(searchMap.get("isSuperadditionid"))){
                criteria.andLike("isSuperadditionid","%"+searchMap.get("isSuperadditionid")+"%");
            }
            // 用户名
            if(searchMap.get("username")!=null && !"".equals(searchMap.get("username"))){
                criteria.andLike("username","%"+searchMap.get("username")+"%");
            }
            // 逻辑删除:0未删除,1已删除
                criteria.andNotEqualTo("isDelete","1");
            // 是否匿名:0不匿名1匿名
            if(searchMap.get("isHide")!=null && !"".equals(searchMap.get("isHide"))){
                criteria.andLike("isHide","%"+searchMap.get("isHide")+"%");
            }

            // 数字1-5星
            if(searchMap.get("userEvaluation")!=null ){
                criteria.andEqualTo("userEvaluation",searchMap.get("userEvaluation"));
            }
            // 点赞数
            if(searchMap.get("praiseNum")!=null ){
                criteria.andEqualTo("praiseNum",searchMap.get("praiseNum"));
            }
            // 回复数量
            if(searchMap.get("replyNum")!=null ){
                criteria.andEqualTo("replyNum",searchMap.get("replyNum"));
            }

        }
        return example;
    }

}

package com.qingcheng.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qingcheng.dao.CommentAdminMapper;
import com.qingcheng.dao.CommentMapper;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.comment.Comment;
import com.qingcheng.pojo.comment.CommentAdmin;
import com.qingcheng.service.comment.CommentAdminService;
import com.qingcheng.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class CommentAdminServiceImpl implements CommentAdminService {

    @Autowired
    private CommentAdminMapper commentAdminMapper;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private CommentMapper commentMapper;
    /**
     * 返回全部记录
     * @return
     */
    @Override
    public List<CommentAdmin> findAll() {
        return commentAdminMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    @Override
    public PageResult<CommentAdmin> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<CommentAdmin> commentAdmins = (Page<CommentAdmin>) commentAdminMapper.selectAll();
        return new PageResult<CommentAdmin>(commentAdmins.getTotal(),commentAdmins.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    @Override
    public List<CommentAdmin> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return commentAdminMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageResult<CommentAdmin> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<CommentAdmin> commentAdmins = (Page<CommentAdmin>) commentAdminMapper.selectByExample(example);
        return new PageResult<CommentAdmin>(commentAdmins.getTotal(),commentAdmins.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    @Override
    public CommentAdmin findById(String id) {
        return commentAdminMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param commentAdmin
     */
    @Override
    public void add(CommentAdmin commentAdmin) {
        commentAdminMapper.insert(commentAdmin);
    }

    /**
     * 修改
     * @param commentAdmin
     */
    @Override
    public void update(CommentAdmin commentAdmin) {
        commentAdminMapper.updateByPrimaryKeySelective(commentAdmin);
    }

    /**
     *  删除
     * @param id
     */
    @Override
    public void delete(String id) {
        commentAdminMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void add(String username,String id, String content) {
        CommentAdmin commentAdmin = new CommentAdmin();
        commentAdmin.setId(idWorker.nextId()+"");
        commentAdmin.setAdminId(username);
        commentAdmin.setCreateTime(new Date());
        commentAdmin.setParentId(id);
        commentAdmin.setContent(content);
        commentAdminMapper.insert(commentAdmin);

        Comment comment = new Comment();
        comment.setId(id);
        comment.setIsSuperadditionid("1");
        commentMapper.updateByPrimaryKeySelective(comment);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(CommentAdmin.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 主键id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 父评论id
            if(searchMap.get("parentId")!=null && !"".equals(searchMap.get("parentId"))){
                criteria.andLike("parentId","%"+searchMap.get("parentId")+"%");
            }
            // 回复内容
            if(searchMap.get("content")!=null && !"".equals(searchMap.get("content"))){
                criteria.andLike("content","%"+searchMap.get("content")+"%");
            }

            // 客服id
            if(searchMap.get("adminId")!=null ){
                criteria.andEqualTo("adminId",searchMap.get("adminId"));
            }

        }
        return example;
    }

}

package com.qingcheng.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qingcheng.dao.CommentSonMapper;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.comment.CommentSon;
import com.qingcheng.service.comment.CommentSonService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class CommentSonServiceImpl implements CommentSonService {

    @Autowired
    private CommentSonMapper commentSonMapper;

    /**
     * 返回全部记录
     * @return
     */
    @Override
    public List<CommentSon> findAll() {
        return commentSonMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    @Override
    public PageResult<CommentSon> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<CommentSon> commentSons = (Page<CommentSon>) commentSonMapper.selectAll();
        return new PageResult<CommentSon>(commentSons.getTotal(),commentSons.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    @Override
    public List<CommentSon> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return commentSonMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageResult<CommentSon> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<CommentSon> commentSons = (Page<CommentSon>) commentSonMapper.selectByExample(example);
        return new PageResult<CommentSon>(commentSons.getTotal(),commentSons.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    @Override
    public CommentSon findById(String id) {
        return commentSonMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param commentSon
     */
    @Override
    public void add(CommentSon commentSon) {
        commentSonMapper.insert(commentSon);
    }

    /**
     * 修改
     * @param commentSon
     */
    @Override
    public void update(CommentSon commentSon) {
        commentSonMapper.updateByPrimaryKeySelective(commentSon);
    }

    /**
     *  删除
     * @param id
     */
    @Override
    public void delete(String id) {
        commentSonMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(CommentSon.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 子评论id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 用户名
            if(searchMap.get("username")!=null && !"".equals(searchMap.get("username"))){
                criteria.andLike("username","%"+searchMap.get("username")+"%");
            }
            // 回复对象
            if(searchMap.get("usernameReply")!=null && !"".equals(searchMap.get("usernameReply"))){
                criteria.andLike("usernameReply","%"+searchMap.get("usernameReply")+"%");
            }
            // 父评论id
            if(searchMap.get("parentId")!=null && !"".equals(searchMap.get("parentId"))){
                criteria.andLike("parentId","%"+searchMap.get("parentId")+"%");
            }
            // 评论详情
            if(searchMap.get("commentInfo")!=null && !"".equals(searchMap.get("commentInfo"))){
                criteria.andLike("commentInfo","%"+searchMap.get("commentInfo")+"%");
            }


        }
        return example;
    }

}

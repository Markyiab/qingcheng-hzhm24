package com.qingcheng.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qingcheng.dao.AgainstMapper;
import com.qingcheng.dao.CommentMapper;
import com.qingcheng.entity.PageResult;
import com.qingcheng.pojo.comment.Against;
import com.qingcheng.pojo.comment.AgainstMSG;
import com.qingcheng.pojo.comment.Comment;
import com.qingcheng.service.comment.AgainstService;
import com.qingcheng.service.comment.CommentService;
import com.qingcheng.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class AgainstServiceImpl implements AgainstService {

    @Autowired
    private AgainstMapper againstMapper;

    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentMapper commentMapper;

    /**
     * 返回全部记录
     * @return
     */
    @Override
    public List<Against> findAll() {
        return againstMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    @Override
    public PageResult<Against> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Against> againsts = (Page<Against>) againstMapper.selectAll();
        return new PageResult<Against>(againsts.getTotal(),againsts.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    @Override
    public List<Against> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return againstMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageResult<Against> findPage(Map<String, Object> searchMap, int page, int size) {
        System.out.println(searchMap);
        System.out.println(page);
        System.out.println(size);
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Against> againsts = (Page<Against>) againstMapper.selectByExample(example);
        return new PageResult<Against>(againsts.getTotal(),againsts.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    @Override
    public Against findById(String id) {
        return againstMapper.selectByPrimaryKey(id);
    }
    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * ZXC新增举报//此用户无记录举报，有记录取消举报 PS：审核建议从内存找，审核完清下内存
     * @param against
     */
    @Override
    public void add(Against against) {
        String orderitemId = against.getOrderitemId();
        String username = against.getUsername();
        if (redisTemplate.boundHashOps("A_U").hasKey(orderitemId)) {
            List<AgainstMSG> users = (List<AgainstMSG>) redisTemplate.boundHashOps("A_U").get(orderitemId);
            ArrayList<AgainstMSG> againstMSGS = new ArrayList<>();
            boolean i = true;//默认新增
            for (AgainstMSG user : users) {
                if (user.getUsername().equals(username)) {
                    System.out.println(username + "取消举报");
                    i = false;
                    Example example = new Example(against.getClass());
                    Example.Criteria criteria = example.createCriteria();
                    criteria.andEqualTo("orderitemId", orderitemId);
                    criteria.andEqualTo("username", username);
                    againstMapper.deleteByExample(example);
                    break;
                } else {
                    i = true;
                    againstMSGS.add(user);
                }
            }
            if (i) {
                System.out.println("新增举报");
                AgainstMSG againstMSG = new AgainstMSG();
                Date date = new Date();
                against.setAuditTime(date);
                againstMSG.setDate(date);
                IdWorker idWorker = new IdWorker();
                String s = Long.toString(idWorker.nextId());
                against.setId(s);
                againstMSG.setAgainstid(s);
                against.setStatus("0");
                againstMSG.setUsername(username);
                againstMSGS.add(againstMSG);
                againstMapper.insert(against);
            }
            if (againstMSGS.size() <= 0) {
                redisTemplate.boundHashOps("A_U").delete(orderitemId);
            } else {
                redisTemplate.boundHashOps("A_U").put(orderitemId, againstMSGS);
            }
        }else {
            ArrayList<AgainstMSG> againstMSGS = new ArrayList<AgainstMSG>();
            AgainstMSG againstMSG = new AgainstMSG();
            Date date = new Date();
            against.setAuditTime(date);
            againstMSG.setDate(date);
            IdWorker idWorker = new IdWorker();
            String s = Long.toString(idWorker.nextId());
            against.setId(s);
            againstMSG.setAgainstid(s);
            against.setStatus("0");
            againstMSG.setUsername(username);
            againstMSGS.add(againstMSG);
            againstMapper.insert(against);
            redisTemplate.boundHashOps("A_U").put(orderitemId, againstMSGS);
        }
    }

    /**
     * 修改
     * @param against
     */
    @Override
    public void update(Against against) {
        againstMapper.updateByPrimaryKeySelective(against);
    }

    /**
     *  删除
     * @param id
     */
    @Override
    public void delete(String id) {
        againstMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(String id,String orderitemId, String status) {
        Against against = new Against();
        against.setId(id);
        against.setAuditTime(new Date());
        against.setStatus(status);
        againstMapper.updateByPrimaryKeySelective(against);
        if (status.equals("1")){
            Comment comment = commentService.findById(orderitemId);
            System.out.println(comment);
            comment.setIsShow("0");
            commentMapper.updateByPrimaryKeySelective(comment);
        }
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Against.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 投诉编号
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andEqualTo("id",searchMap.get("id"));
            }
            // 发起用户
            if(searchMap.get("username")!=null && !"".equals(searchMap.get("username"))){
                criteria.andEqualTo("username",searchMap.get("username"));
            }
            // 被投诉评论id
            if(searchMap.get("orderitemId")!=null && !"".equals(searchMap.get("orderitemId"))){
                criteria.andLike("orderitemId","%"+searchMap.get("orderitemId")+"%");
            }
            // 理由
            if(searchMap.get("reason")!=null && !"".equals(searchMap.get("reason"))){
                criteria.andLike("reason","%"+searchMap.get("reason")+"%");
            }
            // 审核状态0未审核,1审核隐藏,2驳回
            if(searchMap.get("status")!=null && !"".equals(searchMap.get("status"))){
                criteria.andEqualTo("status",searchMap.get("status"));
            }
        }
        return example;
    }

}

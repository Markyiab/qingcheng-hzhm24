package com.qingcheng.dao;

import com.qingcheng.pojo.comment.Comment;
import tk.mybatis.mapper.common.Mapper;

public interface CommentMapper extends Mapper<Comment> {

}

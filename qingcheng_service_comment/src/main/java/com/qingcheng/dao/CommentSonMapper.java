package com.qingcheng.dao;

import com.qingcheng.pojo.comment.CommentSon;
import tk.mybatis.mapper.common.Mapper;

public interface CommentSonMapper extends Mapper<CommentSon> {

}

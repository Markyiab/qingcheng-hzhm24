package com.qingcheng.dao;

import com.qingcheng.pojo.comment.CommentAdmin;
import tk.mybatis.mapper.common.Mapper;

public interface CommentAdminMapper extends Mapper<CommentAdmin> {

}

/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50540
 Source Host           : localhost:3306
 Source Schema         : qingcheng_comment

 Target Server Type    : MySQL
 Target Server Version : 50540
 File Encoding         : 65001

 Date: 29/07/2019 15:49:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_against
-- ----------------------------
DROP TABLE IF EXISTS `tb_against`;
CREATE TABLE `tb_against`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '投诉编号',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发起用户',
  `orderitem_Id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '被投诉评论id',
  `reason` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '理由',
  `create_time` datetime DEFAULT NULL COMMENT '生成时间',
  `audit_time` datetime DEFAULT NULL COMMENT '审核时间',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核状态0未审核,1审核隐藏,2驳回',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_against
-- ----------------------------
INSERT INTO `tb_against` VALUES ('1', '张三', '11', '质量差', '2019-07-03 16:27:49', '2019-07-04 16:27:58', '1');
INSERT INTO `tb_against` VALUES ('10', '卡尔', '43', '质量差', '2019-07-03 17:02:45', '2019-07-28 17:03:17', '1');
INSERT INTO `tb_against` VALUES ('11', '凯莎', '41', '质量差', '2019-07-03 17:02:47', '2019-07-27 17:03:15', '0');
INSERT INTO `tb_against` VALUES ('12', '锤石', '22', '质量差', '2019-07-26 17:02:52', '2019-07-26 17:03:21', '1');
INSERT INTO `tb_against` VALUES ('13', '德莱文', '12', '外观丑', '2019-07-23 17:02:54', '2019-07-29 17:03:23', NULL);
INSERT INTO `tb_against` VALUES ('2', '李四', '21', '商品丑', '2019-07-01 16:28:32', '2019-07-01 16:28:41', '0');
INSERT INTO `tb_against` VALUES ('3', '王五', '32', '质量差', '2019-07-03 16:29:28', '2019-07-03 16:29:34', '0');
INSERT INTO `tb_against` VALUES ('4', '柳六', '43', '质量差', '2019-07-08 16:29:56', '2019-07-09 16:30:01', '1');
INSERT INTO `tb_against` VALUES ('5', '赵七', '54', '外观丑', '2019-07-16 16:30:39', '2019-07-22 16:30:47', '1');
INSERT INTO `tb_against` VALUES ('6', '布里茨', '24', '质量差', '2019-07-08 17:02:41', '2019-07-28 17:03:02', '1');
INSERT INTO `tb_against` VALUES ('7', '菲欧娜', '25', '外观丑', '2019-07-01 17:02:43', '2019-07-28 17:03:04', '1');
INSERT INTO `tb_against` VALUES ('8', '戴安娜', '35', '质量差', '2019-06-29 17:02:48', '2019-07-28 17:03:05', '1');
INSERT INTO `tb_against` VALUES ('9', '卡特玲娜', '27', '外观丑', '2019-07-17 17:02:50', '2019-07-27 17:03:11', '2');

-- ----------------------------
-- Table structure for tb_comment
-- ----------------------------
DROP TABLE IF EXISTS `tb_comment`;
CREATE TABLE `tb_comment`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论id（等同订单详情Id）',
  `spu_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'spu的id',
  `sku_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'sku的id',
  `spec` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品规格信息',
  `is_hasimg` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否有图:0无,1有',
  `user_evaluation` int(1) DEFAULT NULL COMMENT '数字1-5星',
  `evaluation_rating` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '评论等级:0差评1中评2好评',
  `create_time` datetime DEFAULT NULL COMMENT '评论创建时间',
  `praise_num` int(20) DEFAULT NULL COMMENT '点赞数',
  `is_show` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '评论是否隐藏:0隐藏,1显示',
  `is_superadditionid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '追评状态:0无,1有追评,2无官方回复3有官方回复',
  `follow_comment` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '将追评设置为JSON',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
  `images` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片地址',
  `comment_content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文字评论',
  `reply_num` int(8) DEFAULT NULL COMMENT '回复数量',
  `is_delete` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '逻辑删除:0未删除,1已删除',
  `is_hide` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否匿名:0不匿名1匿名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_comment
-- ----------------------------
INSERT INTO `tb_comment` VALUES ('1152198082096664576', '10000015453200', '100000022652', 'vivo Y81s 刘海全面屏 3GB+32GB 香槟金 移动联通电信4G手机', NULL, 5, NULL, '2019-07-29 11:20:25', NULL, NULL, NULL, 'dfhdsfzh', 'xxx', '', 'dxgdfgsdzg', NULL, NULL, '0');
INSERT INTO `tb_comment` VALUES ('1153271485381611520', '10000001516600', '100000015158', '华为 HUAWEI 麦芒7 6G+64G 魅海蓝 全网通  前置智慧双摄  移动联通电信4G手机 双卡双待', NULL, 3, NULL, '2019-07-29 11:16:43', NULL, NULL, NULL, NULL, 'xxx', '', '6756786', NULL, NULL, '0');
INSERT INTO `tb_comment` VALUES ('51', '1143709504987336704', '1143709507357118464', '磨砂黑', '1', 5, '2', '2019-07-28 16:33:02', 666, '1', '0', '是', '张飞', NULL, '外观好看', 1060, '0', '0');
INSERT INTO `tb_comment` VALUES ('52', '1143709504987336704', '1143709507357118464', '玫瑰金', '1', 4, '2', '2019-07-09 16:35:39', 530, '1', '1', '是', '关羽', NULL, '手感好', 639, '0', '1');
INSERT INTO `tb_comment` VALUES ('53', '1143709504987336704', '1143709507357118464', '天空蓝', '1', 3, '1', '2019-07-28 16:37:51', 333, '0', '0', '是', '刘备', NULL, '还可以', 351, '0', '0');
INSERT INTO `tb_comment` VALUES ('54', '1143805111378776064', '1143709508535717888', '深灰色', '1', 5, '2', '2019-07-24 16:39:15', 546, '1', '1', '是', '黄忠', NULL, '外观好看', 751, '0', '0');
INSERT INTO `tb_comment` VALUES ('55', '1143709504987336704', '1143709507357118464', '银  色', '1', 5, '2', '2019-07-01 16:40:45', 254, '1', '1', '是', '董卓', NULL, '手感好', 690, '0', '0');
INSERT INTO `tb_comment` VALUES ('56', '1143709504987336704', '1143709507357118464', '银灰色', '1', 4, '1', '2019-07-10 16:52:32', 250, '1', '0', '是', '卢布', NULL, '外观好看', 351, '0', '0');
INSERT INTO `tb_comment` VALUES ('57', '1143709504987336704', '1143709507357118464', '天空蓝', '1', 4, '1', '2019-07-29 16:55:31', 432, '1', '2', '是', '黄忠', NULL, '手感好', 2131, '0', '0');
INSERT INTO `tb_comment` VALUES ('58', '1143709504987336704', '1143709508535717888', '玫瑰金', '1', 5, '2', '2019-07-09 16:55:11', 321, '0', '3', '是', '曹操', NULL, '外观好看', 231, '0', '0');
INSERT INTO `tb_comment` VALUES ('59', '1143805111378776064', '1143709507357118464', '银灰色', '1', 5, '2', '2019-07-09 16:55:15', 235, '0', '2', '是', '贾宇', NULL, '手感好', 352, '0', '1');
INSERT INTO `tb_comment` VALUES ('60', '1143709504987336704', '1143709507357118464', '磨砂黑', '1', 4, '1', '2019-07-11 16:55:16', 254, '1', '0', '是', '诸葛亮', NULL, '外观好看', 231, '0', '1');
INSERT INTO `tb_comment` VALUES ('61', '1143709504987336704', '1143709507357118464', '玫瑰金', '1', 5, '2', '2019-07-07 16:55:21', 231, '0', '3', '是', '熏悟空', NULL, '外观好看', 234, '0', '1');
INSERT INTO `tb_comment` VALUES ('62', '1143805111378776064', '1143709508535717888', '银  色', '1', 5, '2', '2019-07-05 16:55:18', 646, '1', '1', '是', '虾悟净', NULL, '手感好', 123, '1', '1');
INSERT INTO `tb_comment` VALUES ('63', '1143709504987336704', '1143709507357118464', '磨砂黑', '1', 5, '2', '2019-07-24 16:55:23', 435, '1', '0', '是', '支八戒', NULL, '还可以', 432, '0', '0');
INSERT INTO `tb_comment` VALUES ('64', '1143709504987336704', '1143709508535717888', '玫瑰金', '1', 4, '1', '2019-07-10 16:55:26', 463, '1', '0', '是', '郭嘉', NULL, '外观好看', 624, '0', '0');
INSERT INTO `tb_comment` VALUES ('65', '1143709504987336704', '1143709507357118464', '磨砂黑', '1', 5, '2', '2019-07-24 16:55:28', 234, '1', '3', '是', '孙权', NULL, '外观好看', 521, '0', NULL);

-- ----------------------------
-- Table structure for tb_comment_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_comment_admin`;
CREATE TABLE `tb_comment_admin`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `parent_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父评论id',
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '回复内容',
  `admin_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '客服id',
  `create_time` datetime DEFAULT NULL COMMENT '评论创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_comment_admin
-- ----------------------------
INSERT INTO `tb_comment_admin` VALUES ('61', '121', '外观好看', '9527', '2019-07-28 16:42:08');
INSERT INTO `tb_comment_admin` VALUES ('62', '122', '手感好 ', '1438', '2019-07-10 16:42:29');
INSERT INTO `tb_comment_admin` VALUES ('63', '123', '外观好看', '6666', '2019-07-06 16:42:55');
INSERT INTO `tb_comment_admin` VALUES ('64', '124', '外观好看', '111', '2019-08-01 16:43:21');
INSERT INTO `tb_comment_admin` VALUES ('65', '125', '外观好看', '252', '2019-07-17 17:05:33');
INSERT INTO `tb_comment_admin` VALUES ('66', '126', '手感好 ', '9527', '2019-07-15 17:05:43');
INSERT INTO `tb_comment_admin` VALUES ('67', '127', '外观好看', '1231', '2019-07-12 17:05:44');
INSERT INTO `tb_comment_admin` VALUES ('68', '128', '外观好看', '2135', '2019-07-18 17:05:48');
INSERT INTO `tb_comment_admin` VALUES ('69', '129', '手感好 ', '231', '2019-07-06 17:05:45');
INSERT INTO `tb_comment_admin` VALUES ('70', '130', '外观好看', '1231', '2019-06-29 17:05:47');
INSERT INTO `tb_comment_admin` VALUES ('71', '131', '手感好 ', '1256', '2019-07-10 17:05:50');
INSERT INTO `tb_comment_admin` VALUES ('72', '132', '外观好看', '1235', '2019-07-06 17:05:53');
INSERT INTO `tb_comment_admin` VALUES ('73', '133', '手感好 ', '1231', '2019-07-24 17:05:51');
INSERT INTO `tb_comment_admin` VALUES ('74', '134', '手感好 ', '4123', '2019-07-21 17:05:55');
INSERT INTO `tb_comment_admin` VALUES ('75', '135', '外观好看', '5612', '2019-07-28 17:05:57');

-- ----------------------------
-- Table structure for tb_comment_son
-- ----------------------------
DROP TABLE IF EXISTS `tb_comment_son`;
CREATE TABLE `tb_comment_son`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '子评论id',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `username_reply` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '回复对象',
  `create_time` datetime DEFAULT NULL COMMENT '评论时间',
  `parent_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父评论id',
  `comment_info` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '评论详情',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_comment_son
-- ----------------------------
INSERT INTO `tb_comment_son` VALUES ('201', '乔峰', '扫地僧', '2019-07-30 17:06:59', '153', '外观好看');
INSERT INTO `tb_comment_son` VALUES ('211', '王京', '成龙', '2019-07-29 17:07:01', '154', '手感好');
INSERT INTO `tb_comment_son` VALUES ('212', '李小龙', '蔡徐坤', '2019-07-25 17:07:02', '155', '外观好看');
INSERT INTO `tb_comment_son` VALUES ('213', '谢广坤', '李晨', '2019-07-09 17:07:04', '156', '手感好');
INSERT INTO `tb_comment_son` VALUES ('215', '王宝强', '沈腾', '2019-07-19 17:07:06', '157', '外观好看');
INSERT INTO `tb_comment_son` VALUES ('216', '包贝尔', '王祖蓝', '2019-07-13 17:07:09', '158', '手感好');
INSERT INTO `tb_comment_son` VALUES ('218', '赵四', '赵本山', '2019-07-07 17:07:11', '159', '外观好看');
INSERT INTO `tb_comment_son` VALUES ('221', '李青', '巴德', '2019-07-16 17:17:09', '231', '外观好看');
INSERT INTO `tb_comment_son` VALUES ('89', '虾悟净', '熏悟空', '2019-07-03 16:44:54', '142', '好嗨哟');
INSERT INTO `tb_comment_son` VALUES ('91', '白骨精', '蜘蛛精', '2019-07-01 16:45:28', '146', '手感好');
INSERT INTO `tb_comment_son` VALUES ('92', '皮皮虾', '支八戒', '2019-07-09 16:46:10', '148', '外观好看');
INSERT INTO `tb_comment_son` VALUES ('95', '苏克', '贝塔', '2019-07-16 16:47:31', '149', '还可以');
INSERT INTO `tb_comment_son` VALUES ('96', '飞科', '卢本伟', '2019-07-17 16:48:12', '147', '五五开');
INSERT INTO `tb_comment_son` VALUES ('97', '亚索', '阿托菲斯', '2019-07-10 17:06:51', '150', '手感好');
INSERT INTO `tb_comment_son` VALUES ('98', '易', '瑞兹', '2019-07-06 17:06:57', '151', '外观好看');
INSERT INTO `tb_comment_son` VALUES ('99', '虚竹', '段誉', '2019-07-02 17:06:54', '152', '手感好');

SET FOREIGN_KEY_CHECKS = 1;

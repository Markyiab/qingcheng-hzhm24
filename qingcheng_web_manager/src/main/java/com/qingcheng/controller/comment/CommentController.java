package com.qingcheng.controller.comment;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.entity.PageResult;
import com.qingcheng.entity.Result;
import com.qingcheng.pojo.comment.Comment;
import com.qingcheng.service.comment.CommentService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Reference
    private CommentService commentService;

    @GetMapping("/praise")
    public void praise(String id,String status){

    }

    @GetMapping("/isShow")
    public void isShow(String id,String isShow){
        Comment comment = new Comment();
        comment.setId(id);
        comment.setIsShow(isShow);
        commentService.update(comment);
    }

    @GetMapping("/isDelete")
    public void isDelete(String id,String isDelete){

    }

    @GetMapping("/findAll")
    public List<Comment> findAll(){
        return commentService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Comment> findPage(int page, int size){
        return commentService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Comment> findList(@RequestBody Map<String,Object> searchMap){
        return commentService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Comment> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  commentService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Comment findById(String id){
        return commentService.findById(id);
    }

    @PostMapping("/add")
    public Result add(@RequestBody Comment comment){
        commentService.add(comment);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Comment comment){
        commentService.update(comment);
        return new Result();
    }

    @GetMapping("/addagain")
    public Result addagain(@RequestBody  String id,String followComment){
        commentService.addagain(id,followComment);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        commentService.delete(id);
        return new Result();
    }

}

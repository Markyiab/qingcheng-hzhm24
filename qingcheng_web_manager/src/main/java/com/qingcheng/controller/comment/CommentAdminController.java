package com.qingcheng.controller.comment;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.entity.PageResult;
import com.qingcheng.entity.Result;
import com.qingcheng.pojo.comment.CommentAdmin;
import com.qingcheng.service.comment.CommentAdminService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/commentAdmin")
public class CommentAdminController {

    @Reference
    private CommentAdminService commentAdminService;

    @GetMapping("/findPage")
    public PageResult<CommentAdmin> findPage(int page, int size){
        return commentAdminService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<CommentAdmin> findList(@RequestBody Map<String,Object> searchMap){
        return commentAdminService.findList(searchMap);
    }

    @GetMapping("/findByParentId")
    public CommentAdmin findByParentId(String parentId) {
       Map<String,Object> map = new HashMap<>();
       map.put("parentId",parentId);
        List<CommentAdmin> list = commentAdminService.findList(map);
        CommentAdmin commentAdmin = list.get(0);
        return commentAdmin;
    }

    @PostMapping("/findPage")
    public PageResult<CommentAdmin> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  commentAdminService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public CommentAdmin findById(String id){
        return commentAdminService.findById(id);
    }


    @GetMapping("/add")
    public Result add(String id,String content){
        String username= SecurityContextHolder.getContext().getAuthentication().getName();
        commentAdminService.add(username,id,content);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody CommentAdmin commentAdmin){
        commentAdminService.update(commentAdmin);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        commentAdminService.delete(id);
        return new Result();
    }

}

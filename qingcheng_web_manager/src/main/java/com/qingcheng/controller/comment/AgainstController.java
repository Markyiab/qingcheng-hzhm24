package com.qingcheng.controller.comment;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.entity.PageResult;
import com.qingcheng.entity.Result;
import com.qingcheng.pojo.comment.Against;
import com.qingcheng.service.comment.AgainstService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/against")
public class AgainstController {

    @Reference
    private AgainstService againstService;

    @GetMapping("/audit")//根据审核修改被举报评论的状态
    public Result audit(String id,String orderitemId,String status){
        againstService.update(id,orderitemId,status);
        return new Result();
    }

    @GetMapping("/findAll")
    public List<Against> findAll(){
        return againstService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Against> findPage(int page, int size){
        return againstService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Against> findList(@RequestBody Map<String,Object> searchMap){
        return againstService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Against> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  againstService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Against findById(String id){
        return againstService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Against against){
        againstService.add(against);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Against against){
        againstService.update(against);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        againstService.delete(id);
        return new Result();
    }

}

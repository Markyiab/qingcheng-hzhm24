package com.qingcheng.controller.comment;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qingcheng.entity.PageResult;
import com.qingcheng.entity.Result;
import com.qingcheng.pojo.comment.CommentSon;
import com.qingcheng.service.comment.CommentSonService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/commentSon")
public class CommentSonController {

    @Reference
    private CommentSonService commentSonService;

    @GetMapping("/findAll")
    public List<CommentSon> findAll(){
        return commentSonService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<CommentSon> findPage(int page, int size){
        return commentSonService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<CommentSon> findList(@RequestBody Map<String,Object> searchMap){
        return commentSonService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<CommentSon> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  commentSonService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public CommentSon findById(String id){
        return commentSonService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody CommentSon commentSon){
        commentSonService.add(commentSon);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody CommentSon commentSon){
        commentSonService.update(commentSon);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        commentSonService.delete(id);
        return new Result();
    }

}
